#include <Gamebuino-Meta.h>


//position horizontalX;
int positionX = 32;
//vitesseY;
int speedX = 1;

int ballSize = 8;

//position verticalY;
int positionY = 32;
//vitesseY;
int speedY = 1;

void setup() 
{
  gb.begin();

}

void loop() 
{
  while (!gb.update());
  gb.display.clear();

  positionX = positionX + speedX;
  positionY = positionY + speedY;

  if(positionX < 0)
  {
    speedX = 1;
  }

  if(positionX >= gb.display.width() - ballSize)
  {
    speedX = -1;
  }


  if(positionY < 0)
  {
    speedY = 1;
  }

  if(positionY >= gb.display.height() - ballSize)
  {
    speedY = -1;
  }
  gb.display.setColor(PURPLE);
  gb.display.print("positionX: ");
  gb.display.print(positionX);
  gb.display.print("\nspeedX: ");
  gb.display.print(speedX);



  gb.display.setColor(YELLOW);
  gb.display.print("\npositionY: ");
  gb.display.print(positionY);
  gb.display.print("\nspeedY: ");
  gb.display.print(speedY);




  gb.display.setColor(RED);
  gb.display.fillRect(positionX, positionY, ballSize, ballSize);
}
