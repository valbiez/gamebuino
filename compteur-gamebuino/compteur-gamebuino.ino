#include <Gamebuino-Meta.h>



//compteur
int counter = 0;

void setup() {
  gb.begin();
}


void loop() {
  while(!gb.update());
  gb.display.clear();

  if(gb.buttons.pressed(BUTTON_UP))
  {
    counter = counter + 1;
    gb.sound.playTick();
  }

  if(gb.buttons.pressed(BUTTON_DOWN))
  {
    counter = counter  - 1;
    //gb.sound.playTick();
    gb.sound.playCancel();
  }

  if(gb.buttons.pressed(BUTTON_MENU))
  {
    counter = 0;
    gb.sound.playTick();//son de la touche;
  }
  // C'est ici que le plus gros du programme se déroule
  
  gb.display.fillRect(0, 0, counter, gb.display.height());


  gb.display.setColor(RED);//couleur du caractère;
  gb.display.setFontSize(8);//taille du caractère;
  gb.display.setCursor(8, 8);//position du caractère;

  gb.display.print(counter);
}
