#include <Gamebuino-Meta.h>


//caractéristique de la balle;
int balle_posX = 30;
int balle_posY = 30;
int balle_speedX = 1;
int balle_speedY = 1;
int balle_Taille = 4;

// Caractéristiques de la raquette;
int raquette1_posX = 10;
int raquette1_posY = 30;

//Dimensions de la raquette;
int raquette_hauteur = 20;
int raquette_largeur = 5;

void setup()
{
  gb.begin();
}

void loop() {
  while (!gb.update());
  gb.display.clear();

  // Contrôles de la raquette1;
  if (gb.buttons.repeat(BUTTON_UP, 0)) 
  {
    raquette1_posY = raquette1_posY - 1;
  }
  
  if (gb.buttons.repeat(BUTTON_DOWN, 0)) 
  {
    raquette1_posY = raquette1_posY + 1;
  }

  //Contrôles de la balle;
  balle_posX = balle_posX + balle_speedX;
  balle_posY = balle_posY + balle_speedY;


  if (balle_posY < 0) 
  {  // Rebond en haut
    balle_speedY = 1;
  }


  if (balle_posY > gb.display.height() - balle_Taille) 
  {  // Rebond en bas
    balle_speedY = -1;
  }


  if (balle_posX > gb.display.width() - balle_Taille) 
  {  // Rebond à droite
    balle_speedX = -1;
  }

// Afficher la balle;
  gb.display.setColor(RED);
  gb.display.fillRect(balle_posX, balle_posY, balle_Taille, balle_Taille);
  // Afficher la raquette;
  gb.display.setColor(PURPLE);
  gb.display.fillRect(raquette1_posX, raquette1_posY, raquette_largeur, raquette_hauteur);
}
